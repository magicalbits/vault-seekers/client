/**
 * Vault Seekers Client
 * Copyright (C) 2012  MagicalBits.xyz
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package xyz.magicalbits.vaultseekers.client;


import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.compression.ZlibCodecFactory;
import io.netty.handler.codec.compression.ZlibWrapper;
import io.netty.handler.ssl.SslContext;
import xyz.magicalbits.vaultseekers.common.MessageDecoder;
import xyz.magicalbits.vaultseekers.common.MessageEncoder;

/** Creates a newly configured {@link ChannelPipeline} for a client-side channel. */
public class NetworkClientInitializer extends ChannelInitializer<SocketChannel> {

  private final SslContext sslCtx;
  private final String host;
  private final int port;

  public NetworkClientInitializer(SslContext sslCtx, String host, int port) {
    this.sslCtx = sslCtx;
    this.host = host;
    this.port = port;
  }

  @Override
  public void initChannel(SocketChannel ch) {
    ChannelPipeline pipeline = ch.pipeline();

    if (sslCtx != null) {
      pipeline.addLast(sslCtx.newHandler(ch.alloc(), host, port));
    }

    // Enable stream compression (you can remove these two if unnecessary)
    pipeline.addLast(ZlibCodecFactory.newZlibEncoder(ZlibWrapper.GZIP));
    pipeline.addLast(ZlibCodecFactory.newZlibDecoder(ZlibWrapper.GZIP));

    // Add the number codec first,
    pipeline.addLast(new MessageDecoder());
    pipeline.addLast(new MessageEncoder());

    // and then business logic.
    pipeline.addLast(new NetworkClientHandler());
  }
}
