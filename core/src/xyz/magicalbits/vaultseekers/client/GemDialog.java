/**
 * Vault Seekers Client
 * Copyright (C) 2024  MagicalBits.xyz
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package xyz.magicalbits.vaultseekers.client;


import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import lombok.extern.log4j.Log4j2;
import xyz.magicalbits.vaultseekers.common.proto.Gem;

@Log4j2
public abstract class GemDialog extends GameDialog {
  protected final List<Texture> gemTextures;
  protected Button confirmButton;

  protected GemDialog(
      Skin skin, Label.LabelStyle labelStyle, Stage stage, List<Texture> gemTextures) {
    super(skin, labelStyle, stage);
    this.gemTextures = gemTextures;
  }

  protected static List<Gem> convertChosenGems(Map<Gem.Color, Integer> gems) {
    List<Gem> converted = new ArrayList<>(gems.size());
    for (Map.Entry<Gem.Color, Integer> entry : gems.entrySet()) {
      converted.add(Gem.newBuilder().setColor(entry.getKey()).setAmount(entry.getValue()).build());
    }
    return converted;
  }

  protected void setTitle() {
    Table titleTable = new Table(skin);
    titleTable.add(new Label("Choose three gems", labelStyle));
    dialog.getContentTable().add(titleTable).row();
  }

  protected Button createConfirmButton() {
    Button button = new Button(skin);
    button.add(new Label("Confirm", labelStyle));
    button.addListener(getConfirmButtonListener(button));
    return button;
  }

  protected abstract InputListener getConfirmButtonListener(Button buttonClicked);
}
