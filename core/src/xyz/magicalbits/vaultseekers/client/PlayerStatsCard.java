/**
 * Vault Seekers Client
 * Copyright (C) 2024  MagicalBits.xyz
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package xyz.magicalbits.vaultseekers.client;


import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import java.util.List;
import lombok.Getter;
import xyz.magicalbits.vaultseekers.common.proto.Gem;

public class PlayerStatsCard {
  private final Player player;
  private final Skin skin;
  private final Label.LabelStyle labelStyle;
  private final List<Texture> gemTextures;
  private final Texture hiddenLootCardTexture;
  private Table gemTable;
  private Table otherStatsTable;
  private boolean initialized = false;
  @Getter private Table rootTable;

  public PlayerStatsCard(
      Player player,
      Skin skin,
      Label.LabelStyle labelStyle,
      List<Texture> gemTextures,
      Texture hiddenLootCardTexture) {
    this.player = player;
    this.skin = skin;
    this.labelStyle = labelStyle;
    this.gemTextures = gemTextures;
    this.hiddenLootCardTexture = hiddenLootCardTexture;
  }

  private void init() {
    if (!initialized) {
      rootTable = new Table(skin);

      Table titleTable = new Table(skin);
      titleTable.add(new Label(player.getColor().toString(), labelStyle));

      gemTable = new Table(skin);
      updateGems();

      otherStatsTable = new Table(skin);
      updateOtherStats();

      rootTable.add(titleTable);
      rootTable.row();
      rootTable.add(gemTable).pad(10f, 15f, 10f, 0f);
      rootTable.row();
      rootTable.add(otherStatsTable);

      initialized = true;
    }
  }

  public void update() {
    init();
    updateGems();
    updateOtherStats();
  }

  private void updateGems() {
    gemTable.clear();
    int lastIdx = gemTextures.size() - 1;
    for (int i = 0; i < gemTextures.size(); i++) {
      Table singleGem = new Table(skin);
      Label countLabel =
          new Label(
              String.valueOf(player.getGems().getOrDefault(Gem.Color.forNumber(i), 0)), labelStyle);
      singleGem.add(new Image(gemTextures.get(i)));
      singleGem.row();
      singleGem.add(countLabel);

      Cell<Table> cell = gemTable.add(singleGem);
      if (i != lastIdx) {
        cell.padRight(20f);
      }
    }
  }

  private void updateOtherStats() {
    otherStatsTable.clear();
    otherStatsTable
        .add(new Label("Pts %d".formatted(player.getPoints()), labelStyle))
        .padRight(25f);
    Table hlcGroup = new Table(skin);
    hlcGroup.add(new Image(hiddenLootCardTexture)).padRight(10f);
    hlcGroup.add(new Label("%d".formatted(player.getTotalHiddenLootCardCount()), labelStyle));
    otherStatsTable.add(hlcGroup);
  }
}
