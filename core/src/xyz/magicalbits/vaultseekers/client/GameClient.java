/**
 * Vault Seekers Client
 * Copyright (C) 2022  MagicalBits.xyz
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package xyz.magicalbits.vaultseekers.client;


import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.widget.VisTable;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import xyz.magicalbits.vaultseekers.common.Location;
import xyz.magicalbits.vaultseekers.common.proto.Gem;
import xyz.magicalbits.vaultseekers.common.proto.Message;

@Log4j2
public final class GameClient extends Game {
  /** Subtracting by one to ignore {@link Gem.Color#UNRECOGNIZED} */
  public static final int GEM_COLOR_COUNT = Gem.Color.values().length - 1;

  Stage gameStage;
  Stage titleScreenStage;
  Stage exitStage;
  Stage connectionScreenStage;
  Skin skin;
  Table vaultCardsTable;
  Table playingCardsTable;
  Table extraBossTable;
  Texture blankPlayingCard;
  Button endTurnButton;
  Button leaveGameButton;
  Table endTurnButtonTable;
  Table localPlayerStatsTable;
  RemotePlayerStatsTable remotePlayerStatsTable;
  VisTable gameEndTable;
  List<Texture> gemTextures = new ArrayList<>();
  Texture hiddenLootCardTexture;
  BitmapFont font;
  Label.LabelStyle blackLabelStyle;
  Label.LabelStyle whiteLabelStyle;
  FreeTypeFontGenerator fontGenerator;

  Button playButton;
  Button exitButton;
  Label gameTitle;
  Table titleScreenTable;

  Table connectionScreenTable;
  Button connectButton;
  Button cancelButton;
  TextField serverHostTextField;
  TextField serverPortTextField;
  Label waitingLabel;
  Table waitingLabelTable;
  Button startGameEarlyButton;

  @Getter @Setter private static NetworkClient networkClient;
  static final BlockingQueue<Message> msgQueue = new LinkedBlockingQueue<>();

  static final int MAX_LOCATION_COUNT = 8;
  List<Location> locations = new ArrayList<>(MAX_LOCATION_COUNT);
  Message.PlayerColor color;

  @Override
  public void create() {
    VisUI.load();

    blankPlayingCard = new Texture("cards/playing/playing_blank.png");
    skin = new Skin(Gdx.files.internal("skin/uiskin.json"));

    endTurnButtonTable = new Table(skin);
    endTurnButtonTable.setFillParent(true);
    endTurnButtonTable.top().right();
    endTurnButtonTable.padTop(20f).padRight(20f);
    endTurnButtonTable.setVisible(false);
    endTurnButton = new Button(skin);
    endTurnButton.add(new Label("End Turn", skin));
    endTurnButton.addListener(getEndTurnButtonListener());
    endTurnButtonTable.add(endTurnButton);

    leaveGameButton = new Button(skin);
    leaveGameButton.add(new Label("Leave Game", skin));
    leaveGameButton.addListener(getExitButtonListener());

    playingCardsTable = new Table(skin);
    playingCardsTable.setFillParent(true);
    playingCardsTable.bottom();
    playingCardsTable.padBottom(15f);

    vaultCardsTable = new Table(skin);
    vaultCardsTable.setFillParent(true);
    vaultCardsTable.center();
    vaultCardsTable.padTop(30f);

    extraBossTable = new Table(skin);
    extraBossTable.setFillParent(true);
    extraBossTable.bottom().left();
    extraBossTable.padBottom(15f).padLeft(15f);

    localPlayerStatsTable = new Table(skin);
    localPlayerStatsTable.setFillParent(true);
    localPlayerStatsTable.bottom().right();
    localPlayerStatsTable.padBottom(15f).padRight(30f);

    gameEndTable = new VisTable();
    gameEndTable.setSkin(skin);
    gameEndTable.setFillParent(true);
    gameEndTable.center();
    gameEndTable.setVisible(false);

    FreeTypeFontGenerator.FreeTypeFontParameter parameter =
        new FreeTypeFontGenerator.FreeTypeFontParameter();
    parameter.size = 35;
    fontGenerator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/DroidSerif-Regular.ttf"));
    font = fontGenerator.generateFont(parameter);
    fontGenerator.dispose();
    blackLabelStyle = new Label.LabelStyle(font, Color.BLACK);
    whiteLabelStyle = new Label.LabelStyle(font, Color.WHITE);

    hiddenLootCardTexture = new Texture(Gdx.files.internal("icons/hidden_loot.png"));

    remotePlayerStatsTable =
        new RemotePlayerStatsTable(skin, blackLabelStyle, gemTextures, hiddenLootCardTexture);
    remotePlayerStatsTable.setFillParent(true);
    remotePlayerStatsTable.center().left();

    // TODO: consider rewriting the end game screen to a separate class tied to this stage
    exitStage = new Stage();

    waitingLabelTable = new Table(skin);
    waitingLabelTable.setFillParent(true);
    waitingLabelTable.center();
    waitingLabel = new Label("Waiting for other players ... (x/y)", blackLabelStyle);
    waitingLabel.setVisible(false);
    startGameEarlyButton = new Button(skin);
    startGameEarlyButton.setDisabled(true);
    startGameEarlyButton.setVisible(false);
    startGameEarlyButton.add(new Label("Start game", blackLabelStyle));
    startGameEarlyButton.addListener(getStartGameEarlyButtonListener());
    waitingLabelTable.add(waitingLabel);
    waitingLabelTable.row();
    waitingLabelTable.add(startGameEarlyButton);

    gameStage = new Stage();
    gameStage.addActor(playingCardsTable);
    gameStage.addActor(vaultCardsTable);
    gameStage.addActor(extraBossTable);
    gameStage.addActor(endTurnButtonTable);
    gameStage.addActor(localPlayerStatsTable);
    gameStage.addActor(remotePlayerStatsTable);
    gameStage.addActor(gameEndTable);
    gameStage.addActor(waitingLabelTable);

    // title screen elements
    gameTitle = new Label("Vault Seekers", blackLabelStyle);
    playButton = new Button(skin);
    playButton.add(new Label("Play", blackLabelStyle));
    playButton.addListener(getPlayButtonListener());
    exitButton = new Button(skin);
    exitButton.add(new Label("Exit", blackLabelStyle));
    exitButton.addListener(getExitButtonListener());

    titleScreenTable = new Table(skin);
    titleScreenTable.setFillParent(true);
    titleScreenTable.center();
    titleScreenTable.add(gameTitle).row();
    titleScreenTable.add(playButton).padBottom(10f).row();
    titleScreenTable.add(exitButton);

    titleScreenStage = new Stage();
    titleScreenStage.addActor(titleScreenTable);

    connectionScreenTable = new Table(skin);
    connectionScreenTable.setFillParent(true);
    connectionScreenTable.center();
    connectionScreenTable.add(new Label("Server host:", blackLabelStyle));
    serverHostTextField = new TextField("", skin);
    connectionScreenTable.add(serverHostTextField).padLeft(30f);
    connectionScreenTable.row();
    connectionScreenTable.add(new Label("Server port:", blackLabelStyle));
    serverPortTextField = new TextField("", skin);
    connectionScreenTable.add(serverPortTextField).padLeft(30f);
    connectionScreenTable.row();
    connectButton = new Button(skin);
    connectButton.add(new Label("Connect", blackLabelStyle));
    connectButton.addListener(getConnectButtonListener());
    cancelButton = new Button(skin);
    cancelButton.add(new Label("Cancel", blackLabelStyle));
    cancelButton.addListener(getCancelButtonListener());
    connectionScreenTable.add(connectButton);
    connectionScreenTable.add(cancelButton);

    connectionScreenStage = new Stage();
    connectionScreenStage.addListener(getConnectionScreenListener());
    connectionScreenStage.addActor(connectionScreenTable);

    setScreen(new TitleScreen(this));
  }

  private InputListener getStartGameEarlyButtonListener() {
    return new InputListener() {
      @Override
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        return true;
      }

      @Override
      public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        if (!startGameEarlyButton.isDisabled()) {
          startGameEarlyButton.setDisabled(true);
          startGameEarlyButton.setVisible(false);
          log.debug("Sending message of type {} to the server.", Message.Type.START_GAME);
          GameClient.getNetworkClient()
              .sendMessage(Message.newBuilder().setType(Message.Type.START_GAME).build());
        }
      }
    };
  }

  @Override
  public void dispose() {
    if (networkClient != null) {
      networkClient.dispose();
    }
    skin.dispose();
    gameStage.dispose();
    titleScreenStage.dispose();
    exitStage.dispose();
    connectionScreenStage.dispose();
    blankPlayingCard.dispose();
    gemTextures.forEach(Texture::dispose);
    font.dispose();
    hiddenLootCardTexture.dispose();
    screen.dispose();
  }

  private InputListener getEndTurnButtonListener() {
    return new InputListener() {
      @Override
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        return true;
      }

      @Override
      public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        super.touchUp(event, x, y, pointer, button);
        log.debug(
            "End turn button clicked. {} {} {} {} {}",
            x,
            y,
            pointer,
            button,
            endTurnButton.isDisabled());

        float buttonWidth = endTurnButton.getWidth();
        float buttonHeight = endTurnButton.getHeight();
        if (x >= 0 && x <= buttonWidth && y >= 0 && y <= buttonHeight) {
          // If the player clicked and released the button with the mouse cursor within the
          // button, hide the cards in hand and send the chosen att/def cards to the server.

          playingCardsTable.setVisible(false);
          endTurnButtonTable.setVisible(false);

          List<Message.Location> remoteLocations = new ArrayList<>(locations.size());
          for (Location loc : locations) {
            remoteLocations.add(
                Message.Location.newBuilder()
                    .setName(loc.getName().toString())
                    .addAllAttackCards(
                        loc.getAttackCards().stream()
                            .map(
                                card ->
                                    Message.Card.newBuilder()
                                        .setPath(card.getFilePath())
                                        .setColor(card.getColor())
                                        .build())
                            .toList())
                    .addAllDefenseCards(
                        loc.getDefenseCards().stream()
                            .map(
                                card ->
                                    Message.Card.newBuilder()
                                        .setPath(card.getFilePath())
                                        .setColor(card.getColor())
                                        .build())
                            .toList())
                    .build());
          }

          networkClient.sendMessage(
              Message.newBuilder()
                  .setType(Message.Type.UPDATE_LOCATIONS)
                  .addAllLocations(remoteLocations)
                  .build());
        }
      }
    };
  }

  private InputListener getPlayButtonListener() {
    return new InputListener() {
      @Override
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        return true;
      }

      @Override
      public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        setScreen(new ConnectionScreen(GameClient.this));
      }
    };
  }

  private InputListener getExitButtonListener() {
    return new InputListener() {
      @Override
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        return true;
      }

      @Override
      public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        Gdx.app.exit();
      }
    };
  }

  private InputListener getConnectButtonListener() {
    return new InputListener() {
      @Override
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        return true;
      }

      @Override
      public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        connect();
      }
    };
  }

  private InputListener getCancelButtonListener() {
    return new InputListener() {
      @Override
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        return true;
      }

      @Override
      public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        setScreen(new TitleScreen(GameClient.this));
      }
    };
  }

  private InputListener getConnectionScreenListener() {
    return new InputListener() {
      @Override
      public boolean keyUp(InputEvent event, int keycode) {
        if (keycode == Input.Keys.ENTER) {
          connect();
        }
        return true;
      }
    };
  }

  private void connect() {
    final String hostname = serverHostTextField.getText();

    // Hostname validation
    if (hostname.isBlank()) {
      showErrorDialog("Server hostname is blank", connectionScreenStage);
      return;
    }

    // Port validation
    int port;
    try {
      port = Integer.parseInt(serverPortTextField.getText());
    } catch (NumberFormatException e) {
      showErrorDialog("Invalid server port", connectionScreenStage);
      return;
    }
    if (port <= 0 || port > 65535) {
      showErrorDialog("Invalid server port", connectionScreenStage);
      return;
    }

    log.info("Connecting to {}:{}", hostname, port);

    // Establish server connection
    NetworkClient nc;
    try {
      nc = new NetworkClient(hostname, port);
    } catch (UnknownHostException e) {
      String message = "Couldn't connect to server";
      showErrorDialog(message, connectionScreenStage);
      log.error("{} '{}:{}'", message, hostname, port);
      return;
    }
    GameClient.setNetworkClient(nc);

    setScreen(new GameScreen(GameClient.this));
  }

  private void showErrorDialog(String message, Stage stage) {
    new ErrorDialog(skin, whiteLabelStyle, stage, message).show();
  }
}
