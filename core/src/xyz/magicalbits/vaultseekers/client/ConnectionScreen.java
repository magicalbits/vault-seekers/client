/**
 * Vault Seekers Client
 * Copyright (C) 2023  MagicalBits.xyz
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package xyz.magicalbits.vaultseekers.client;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.utils.ScreenUtils;

public class ConnectionScreen extends ScreenAdapter {

  private final GameClient client;

  public ConnectionScreen(GameClient client) {
    this.client = client;
  }

  @Override
  public void render(float delta) {
    ScreenUtils.clear(1, 1, 1, 1);

    client.connectionScreenStage.act(delta);
    client.connectionScreenStage.draw();
  }

  @Override
  public void show() {
    Gdx.input.setInputProcessor(client.connectionScreenStage);
    client.connectionScreenStage.setKeyboardFocus(client.serverHostTextField);
    client.connectionScreenTable.setVisible(true);
  }

  @Override
  public void hide() {
    Gdx.input.setInputProcessor(null);
    client.connectionScreenTable.setVisible(false);
  }
}
