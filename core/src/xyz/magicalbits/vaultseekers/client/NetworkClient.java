/**
 * Vault Seekers Client
 * Copyright (C) 2023  MagicalBits.xyz
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package xyz.magicalbits.vaultseekers.client;


import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.ssl.SslContext;
import java.net.UnknownHostException;
import java.security.cert.CertificateException;
import javax.net.ssl.SSLException;
import lombok.extern.log4j.Log4j2;
import xyz.magicalbits.vaultseekers.common.proto.Message;
import xyz.magicalbits.vaultseekers.common.util.ServerUtil;

@Log4j2
public class NetworkClient {
  NetworkClientHandler handler;
  EventLoopGroup group;

  public NetworkClient(String host, int port) throws UnknownHostException {
    try {
      // Configure SSL.
      final SslContext sslCtx = ServerUtil.buildSslContext();

      group = new NioEventLoopGroup();

      Bootstrap b = new Bootstrap();
      b.group(group)
          .channel(NioSocketChannel.class)
          .handler(new NetworkClientInitializer(sslCtx, host, port));

      // Make a new connection.
      ChannelFuture f = b.connect(host, port).sync();

      // Get the handler instance to exchange data during gameplay.
      handler = (NetworkClientHandler) f.channel().pipeline().last();
    } catch (CertificateException | SSLException e) {
      dispose();
      throw new RuntimeException(e);
    } catch (InterruptedException e) {
      dispose();
      Thread.currentThread().interrupt();
    }

    log.info("Established connection with server {}:{}", host, port);
  }

  public void sendMessage(Message msg) {
    handler.sendMessage(msg);
  }

  public void dispose() {
    group.shutdownGracefully();
  }
}
