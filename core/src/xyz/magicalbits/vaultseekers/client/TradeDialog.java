/**
 * Vault Seekers Client
 * Copyright (C) 2024  MagicalBits.xyz
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package xyz.magicalbits.vaultseekers.client;

import static xyz.magicalbits.vaultseekers.client.GameClient.GEM_COLOR_COUNT;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.extern.log4j.Log4j2;
import xyz.magicalbits.vaultseekers.common.proto.Gem;
import xyz.magicalbits.vaultseekers.common.proto.Message;

@Log4j2
public class TradeDialog extends GemDialog {
  public static final int MAX_TRADE_GEMS = 3;
  public static final float BUTTON_PAD = 10f;
  private final Map<Player, Map<Gem.Color, Integer>> chosenGems = new HashMap<>();
  private final Map<Player, Map<Gem.Color, Pair<Button, Button>>> plusMinusButtons =
      new HashMap<>();
  private final Map<Player, List<Label>> countLabelsMap = new HashMap<>();
  private final List<Pair<Gem.Color, Pair<Player, Gem.Color>>> gemLinks =
      new ArrayList<>(MAX_TRADE_GEMS);
  private final List<Player> allPlayers;
  private final Player localPlayer;

  protected TradeDialog(
      Skin skin,
      Label.LabelStyle labelStyle,
      Stage stage,
      List<Texture> gemTextures,
      List<Player> players,
      Player localPlayer) {
    super(skin, labelStyle, stage, gemTextures);
    this.allPlayers = new ArrayList<>(players);
    this.localPlayer = localPlayer;
  }

  @Override
  protected void show() {
    chosenGems.clear();

    dialog = new Dialog("", skin);
    dialog.setMovable(false);

    setTitle();

    Table playerTables = new Table(skin);

    // Local player is placed first.
    allPlayers.remove(localPlayer);
    allPlayers.add(0, localPlayer);

    for (Player player : allPlayers) {
      Table playerTable = new Table(skin);
      Label playerColor =
          new Label(player.equals(localPlayer) ? "You" : player.getColor().toString(), skin);
      Table gemTable = createGemTable(player);
      playerTable.add(playerColor);
      playerTable.row();
      playerTable.add(gemTable);

      // Pad each player's table unless it's the last table to be displayed.
      if (player == allPlayers.get(allPlayers.size() - 1)) {
        playerTables.add(playerTable);
      } else {
        playerTables.add(playerTable).padRight(30f);
      }
    }
    dialog.getContentTable().add(playerTables);

    confirmButton = createConfirmButton();
    dialog.getButtonTable().add(confirmButton);

    dialog.show(stage);
  }

  protected Table createGemTable(Player player) {
    Table gemTable = new Table(skin);
    List<Label> countLabels = new ArrayList<>(GEM_COLOR_COUNT);
    countLabelsMap.put(player, countLabels);

    // Add plus buttons.
    for (int i = 0; i < GEM_COLOR_COUNT; i++) {
      Button plusButton = new Button(skin);
      Gem.Color gemColor = Gem.Color.values()[i];

      if (player == localPlayer) {
        if (player.getGems().getOrDefault(gemColor, 0) > 0) {
          showButton(plusButton);
        }
      } else {
        hideButton(plusButton);
      }
      plusButton.add(new Label("+", labelStyle)).pad(BUTTON_PAD);
      plusButton.addListener(getGemButtonListener(gemColor, plusButton, true, countLabels, player));
      gemTable.add(plusButton).pad(BUTTON_PAD);

      // Add the button reference to the map.
      Map<Gem.Color, Pair<Button, Button>> gemButtonsMap =
          plusMinusButtons.getOrDefault(player, new EnumMap<>(Gem.Color.class));
      gemButtonsMap.put(gemColor, new Pair<>(plusButton, null));
      plusMinusButtons.put(player, gemButtonsMap);
    }
    gemTable.row();

    // Add gem icons.
    for (int i = 0; i < GEM_COLOR_COUNT; i++) {
      gemTable.add(new Image(gemTextures.get(i))).pad(BUTTON_PAD);
    }
    gemTable.row();

    // Add minus buttons.
    for (int i = 0; i < GEM_COLOR_COUNT; i++) {
      Button minusButton = new Button(skin);
      hideButton(minusButton);
      minusButton.add(new Label("-", labelStyle)).pad(BUTTON_PAD);
      minusButton.addListener(
          getGemButtonListener(Gem.Color.values()[i], minusButton, false, countLabels, player));
      gemTable.add(minusButton);

      // Add the button reference to the map.
      Pair<Button, Button> buttonPair = plusMinusButtons.get(player).get(Gem.Color.values()[i]);
      buttonPair.setSecond(minusButton);
    }
    gemTable.row();

    // Add gem counts.
    for (int i = 0; i < GEM_COLOR_COUNT; i++) {
      Label label =
          new Label(
              "0/%d".formatted(player.getGems().getOrDefault(Gem.Color.forNumber(i), 0)),
              labelStyle);
      gemTable.add(label);
      countLabels.add(label);
    }
    return gemTable;
  }

  /** Calculates the sum of the player's gems which the local player selected for trading. */
  private int getChosenGemsSum(Player p) {
    return chosenGems.getOrDefault(p, new EnumMap<>(Gem.Color.class)).values().stream()
        .reduce(Integer::sum)
        .orElse(0);
  }

  /** Calculates the sum of gems selected from other players, skipping the local player. */
  private int getChosenGemsSum() {
    int sum = 0;
    for (Player p : allPlayers) {
      if (p == localPlayer) {
        continue;
      }
      sum += getChosenGemsSum(p);
    }
    return sum;
  }

  private InputListener getGemButtonListener(
      Gem.Color gemColor,
      Button buttonClicked,
      boolean isPlus,
      List<Label> countLabels,
      Player player) {
    return new InputListener() {
      @Override
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        return true;
      }

      @Override
      public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        log.debug(
            "Gem button '{}' clicked. {} {} {} {} {}",
            gemColor.toString(),
            x,
            y,
            pointer,
            button,
            buttonClicked.isDisabled());

        float buttonWidth = buttonClicked.getWidth();
        float buttonHeight = buttonClicked.getHeight();
        boolean releasedInBounds = x >= 0 && x <= buttonWidth && y >= 0 && y <= buttonHeight;
        if (!releasedInBounds || buttonClicked.isDisabled()) {
          return;
        }

        Map<Gem.Color, Integer> chosenPlayerGems =
            chosenGems.getOrDefault(player, new EnumMap<>(Gem.Color.class));
        chosenGems.putIfAbsent(player, chosenPlayerGems);
        final int oldValue = chosenPlayerGems.getOrDefault(gemColor, 0);
        int playerGemLimit = player.getGems().getOrDefault(gemColor, 0);
        log.debug(
            "player {}; gem {}; oldValue {}; limit {}",
            player.getColor(),
            gemColor,
            oldValue,
            playerGemLimit);

        final int newValue;
        if (isPlus) {
          if (oldValue == playerGemLimit
              || player == localPlayer && getChosenGemsSum(player) == MAX_TRADE_GEMS
              || player != localPlayer && getChosenGemsSum() == MAX_TRADE_GEMS) {
            // Return, if any of the following conditions are true:
            // - the player's gem limit is reached
            // - the local player's chosen gem count equals MAX_TRADE_GEMS
            // - all other players' chosen gem count equals MAX_TRADE_GEMS
            return;
          }
          newValue = oldValue + 1;
          log.debug("selected gem {} of player {}", gemColor, player.getColor());
        } else {
          if (oldValue == 0) {
            // Return if the player attempts to choose less than zero gems.
            return;
          }
          newValue = oldValue - 1;
          log.debug("unselected gem {} of player {}", gemColor, player.getColor());
        }

        if (newValue == 0) {
          chosenPlayerGems.remove(gemColor);
        } else {
          chosenPlayerGems.put(gemColor, newValue);
        }

        // TODO hide local player's minus button if it's clicked when chosen gem count is one,
        //   changing it to zero
        // TODO clicking on local player's minus button must remove the corresponding chosen gem
        //   from another player
        if (isPlus) {
          if (player == localPlayer) {
            // Hide all buttons of the local player
            hideButtons(player, true);
            hideButtons(player, false);

            // Show plus buttons of other players
            allPlayers.stream()
                .filter(p -> !p.equals(localPlayer))
                .forEach(p -> showButtons(p, true));

            // Add first half of the gem link.
            gemLinks.add(new Pair<>(gemColor, new Pair<>()));
          } else {
            // Show all buttons of the local player
            showButtons(localPlayer, true);
            showButtons(localPlayer, false);

            // Hide plus buttons of other players
            allPlayers.stream()
                .filter(p -> !p.equals(localPlayer))
                .forEach(p -> hideButtons(p, true));

            // Add second half of the gem link.
            Pair<Player, Gem.Color> linkTarget = gemLinks.get(gemLinks.size() - 1).getSecond();
            linkTarget.setFirst(player);
            linkTarget.setSecond(gemColor);
          }
        } else if (player == localPlayer) {
          List<Pair<Gem.Color, Pair<Player, Gem.Color>>> reversedGemLinks =
              new ArrayList<>(gemLinks);
          Collections.reverse(reversedGemLinks);
          Pair<Gem.Color, Pair<Player, Gem.Color>> gemLink =
              reversedGemLinks.stream()
                  .filter(pair -> pair.getFirst().equals(gemColor))
                  .findFirst()
                  .orElseThrow(() -> new RuntimeException("Gem link not found."));
          Player targetPlayer = gemLink.getSecond().getFirst();
          Gem.Color targetGemColor = gemLink.getSecond().getSecond();

          Map<Gem.Color, Integer> gems = chosenGems.get(targetPlayer);
          int newLabelValue = gems.get(targetGemColor) - 1;
          gems.put(targetGemColor, newLabelValue);

          countLabelsMap
              .get(targetPlayer)
              .get(targetGemColor.ordinal())
              .setText(
                  "%d/%d"
                      .formatted(
                          newLabelValue, targetPlayer.getGems().getOrDefault(targetGemColor, 0)));

          gemLinks.remove(gemLink);
        }
        // FIXME on table creation, do NOT show plus buttons where the local player has zero gems

        countLabels.get(gemColor.ordinal()).setText("%d/%d".formatted(newValue, playerGemLimit));

        // Disable button if the amount of the local player's offered gems doesn't equal the sum of
        // chosen gems from other players.
        boolean isDisabled = getChosenGemsSum(localPlayer) != getChosenGemsSum();
        confirmButton.setDisabled(isDisabled);
        confirmButton.setVisible(!isDisabled);
      }
    };
  }

  @Override
  protected InputListener getConfirmButtonListener(Button buttonClicked) {
    return new InputListener() {
      @Override
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        return true;
      }

      @Override
      public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        log.debug(
            "Confirm button clicked. {} {} {} {} {}",
            x,
            y,
            pointer,
            button,
            buttonClicked.isDisabled());

        float buttonWidth = buttonClicked.getWidth();
        float buttonHeight = buttonClicked.getHeight();
        if (x >= 0
            && x <= buttonWidth
            && y >= 0
            && y <= buttonHeight
            && !buttonClicked.isDisabled()) {
          buttonClicked.setDisabled(true);
          closeDialog();

          // Send chosen gems to the server.
          GameClient.getNetworkClient()
              .sendMessage(
                  Message.newBuilder()
                      .setType(Message.Type.SHOW_DIALOG)
                      .setDialog(
                          Message.Dialog.newBuilder().setType(Message.Dialog.Type.TRADE).build())
                      .addAllPlayerStats(getAllPlayerStats())
                      .addAllGemLinks(getAllGemLinks())
                      .build());
        }
      }
    };
  }

  private List<Message.PlayerStats> getAllPlayerStats() {
    return allPlayers.stream().map(this::getPlayerStats).toList();
  }

  private Message.PlayerStats getPlayerStats(Player p) {
    return Message.PlayerStats.newBuilder()
        .setColor(p.getColor())
        .addAllGems(convertChosenGems(chosenGems.getOrDefault(p, new EnumMap<>(Gem.Color.class))))
        .build();
  }

  private List<Message.GemLink> getAllGemLinks() {
    return gemLinks.stream()
        .map(
            link -> {
              Player p = link.getSecond().getFirst();
              Gem.Color c = link.getFirst();
              return Message.GemLink.newBuilder()
                  .setTarget(p.getColor())
                  .setSourceGemColor(c)
                  .build();
            })
        .toList();
  }

  private static void showButton(Button b) {
    b.setDisabled(false);
    b.setVisible(true);
  }

  private static void hideButton(Button b) {
    b.setDisabled(true);
    b.setVisible(false);
  }

  private void hideButtons(Player p, boolean isPlus) {
    for (int i = 0; i < GEM_COLOR_COUNT; i++) {
      Pair<Button, Button> pair = plusMinusButtons.get(p).get(Gem.Color.values()[i]);
      Button b;
      if (isPlus) {
        b = pair.getFirst();
      } else {
        b = pair.getSecond();
      }
      hideButton(b);
    }
  }

  private void showButtons(Player p, boolean isPlus) {
    log.debug("showing {} buttons of player {}", isPlus ? "plus" : "minus", p.getColor());
    log.debug("chosenGems map: {}", chosenGems);
    for (int i = 0; i < GEM_COLOR_COUNT; i++) {
      log.debug("showing buttons of gem color {}", Gem.Color.values()[i].toString());

      int gemCount = p.getGems().getOrDefault(Gem.Color.values()[i], 0);
      boolean isGemCountZero = gemCount == 0;
      int chosenGemCount =
          chosenGems
              .getOrDefault(p, new EnumMap<>(Gem.Color.class))
              .getOrDefault(Gem.Color.values()[i], 0);
      boolean isChosenGemCountZero = !isPlus && chosenGemCount == 0;
      boolean isChosenGemCountEqualToLimit = p != localPlayer && chosenGemCount == gemCount;
      if (isGemCountZero || isChosenGemCountZero || isChosenGemCountEqualToLimit) {
        log.debug(
            "skipped showing button: {} {} {}",
            isGemCountZero,
            isChosenGemCountZero,
            isChosenGemCountEqualToLimit);
        continue;
      }

      Pair<Button, Button> pair = plusMinusButtons.get(p).get(Gem.Color.values()[i]);
      Button b;
      if (isPlus) {
        b = pair.getFirst();
      } else {
        b = pair.getSecond();
      }
      showButton(b);
    }
  }
}
