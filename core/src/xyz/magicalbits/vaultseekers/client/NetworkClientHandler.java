/**
 * Vault Seekers Client
 * Copyright (C) 2023  MagicalBits.xyz
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package xyz.magicalbits.vaultseekers.client;


import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.extern.log4j.Log4j2;
import xyz.magicalbits.vaultseekers.common.proto.Message;

/**
 * Handler for a client-side channel. This handler maintains stateful information which is specific
 * to a certain channel using member variables. Therefore, an instance of this handler can cover
 * only one channel. You have to create a new handler instance whenever you create a new channel and
 * insert this handler to avoid a race condition.
 */
@Log4j2
public class NetworkClientHandler extends SimpleChannelInboundHandler<Message> {

  private ChannelHandlerContext ctx;

  @Override
  public void channelActive(ChannelHandlerContext ctx) {
    this.ctx = ctx;
  }

  @Override
  public void channelRead0(ChannelHandlerContext ctx, final Message msg) {
    boolean offered = GameClient.msgQueue.offer(msg);
    assert offered;
    log.info("Received a message of type {}.", msg.getType());
  }

  @Override
  public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
    cause.printStackTrace();
    ctx.close();
  }

  protected void sendMessage(Message msg) {
    ChannelFuture future = ctx.write(msg);
    assert future != null;
    future.addListener(msgSender);
    ctx.flush();
  }

  private final ChannelFutureListener msgSender =
      future -> {
        if (!future.isSuccess()) {
          future.cause().printStackTrace();
          future.channel().close();
        }
      };
}
