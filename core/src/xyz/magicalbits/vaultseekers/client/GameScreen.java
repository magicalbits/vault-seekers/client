/**
 * Vault Seekers Client
 * Copyright (C) 2023  MagicalBits.xyz
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package xyz.magicalbits.vaultseekers.client;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.ScreenUtils;
import com.kotcrab.vis.ui.widget.VisTable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;
import lombok.extern.log4j.Log4j2;
import xyz.magicalbits.vaultseekers.common.Location;
import xyz.magicalbits.vaultseekers.common.card.LocationCard;
import xyz.magicalbits.vaultseekers.common.card.PlayingCard;
import xyz.magicalbits.vaultseekers.common.card.VaultCard;
import xyz.magicalbits.vaultseekers.common.proto.Gem;
import xyz.magicalbits.vaultseekers.common.proto.Message;

@Log4j2
public class GameScreen extends ScreenAdapter {
  private static final float VAULT_CARDS_TABLE_RIGHT_PAD = 50f;
  private static final float GAME_END_TABLE_RIGHT_PAD = 20f;
  private static final int PLAYER_LIMIT = 5;
  private final GameClient client;
  private final List<PlayingCard> playingCardsInHand =
      new ArrayList<>(GameClient.MAX_LOCATION_COUNT);
  private final List<Image> gemIcons = new ArrayList<>();
  private final List<Player> players = new ArrayList<>(PLAYER_LIMIT);
  private Player localPlayer;
  private int placedAttackCardsLimit;
  private int placedDefenseCardsLimit;
  private int playerCount = 0;
  private boolean host = false;

  public GameScreen(GameClient client) {
    this.client = client;
  }

  @Override
  public void resize(int width, int height) {
    client.gameStage.getViewport().update(width, height, true);
  }

  @Override
  public void render(float delta) {
    ScreenUtils.clear(1, 1, 1, 1);

    client.gameStage.act(delta);
    client.gameStage.draw();

    // After the gameStage.draw() above, rendered objects can be manipulated right here.

    Message msg = GameClient.msgQueue.poll();
    if (msg != null) {
      processMessage(msg);
    }
  }

  @Override
  public void show() {
    Gdx.input.setInputProcessor(client.gameStage);
  }

  @Override
  public void hide() {
    Gdx.input.setInputProcessor(null);
  }

  public void processMessage(Message msg) {
    log.info("Processing an incoming message of type {}.", msg.getType());
    switch (msg.getType()) {
      case UPDATE_PLAYER_COUNT -> {
        playerCount = msg.getPlayerStatsCount();
        log.info("Set player count to {}.", playerCount);
        updateWaitingLabel();

        if (host) {
          // Enable and show the button if there are at least two players, otherwise disable and
          // hide it.
          client.startGameEarlyButton.setDisabled(playerCount < 2);
          client.startGameEarlyButton.setVisible(playerCount >= 2);
        }
      }
      case SET_PLAYER_COLOR -> {
        client.waitingLabel.setVisible(false);
        client.color = msg.getPlayerStatsList().get(0).getColor();
        log.info("Set player color to {}.", client.color);
        placePlayingCards();
        initGemIcons();
      }
      case UPDATE_PLAYER_STATS -> {
        for (Message.PlayerStats stats : msg.getPlayerStatsList()) {
          updatePlayer(stats);
        }

        if (localPlayer != null && localPlayer.isUpdated()) {
          updateLocalPlayerStatsTable();
          localPlayer.setUpdated(false);
        }
      }
      case UPDATE_LOCATIONS -> {
        List<Message.Location> locationsList = msg.getLocationsList();
        if (client.locations.isEmpty()) {
          initLocations(locationsList.size());
        }

        for (int i = 0; i < locationsList.size(); i++) {
          Message.Location remoteLoc = locationsList.get(i);
          Location loc = client.locations.get(i);

          if (remoteLoc.getDefenseCardsCount() == 0) {
            loc.setDefenseCards(new ArrayList<>());
            log.info("No defense cards found. Setting empty list.");
          } else {
            log.info(
                "Setting {} defense card{} to location {}.",
                remoteLoc.getDefenseCardsCount(),
                remoteLoc.getDefenseCardsCount() == 1 ? "" : "s",
                remoteLoc.getName());

            loc.setDefenseCards(
                remoteLoc.getDefenseCardsList().stream()
                    .map(card -> new PlayingCard(card.getPath(), card.getColor(), loc.getName()))
                    .toList());
          }

          if (remoteLoc.getAttackCardsCount() == 0) {
            loc.setAttackCards(new ArrayList<>());
            log.info("No attack cards found. Setting empty list.");
          } else {
            log.info(
                "Setting {} attack card{} to location {}.",
                remoteLoc.getAttackCardsCount(),
                remoteLoc.getAttackCardsCount() == 1 ? "" : "s",
                remoteLoc.getName());

            loc.setAttackCards(
                remoteLoc.getAttackCardsList().stream()
                    .map(card -> new PlayingCard(card.getPath(), card.getColor(), loc.getName()))
                    .toList());
          }

          if (remoteLoc.hasVaultCardPath()) {
            // Server sent a vault card -> set the vault card.
            loc.setVaultCard(new VaultCard(new Image(new Texture(remoteLoc.getVaultCardPath()))));
            log.info("Vault card set to '{}'.", remoteLoc.getVaultCardPath());
          } else if (remoteLoc.hasShouldDeleteVaultCard() && remoteLoc.getShouldDeleteVaultCard()) {
            // Server removed this location's vault card during conflict resolution -> delete it.
            loc.setVaultCard(null);
            log.info("Vault card deleted.");
          } else {
            log.info("Vault card unchanged.");
          }
        }

        updateCards();
      }
      case UPDATE_EXTRA_BOSS -> {
        if (msg.hasExtraBoss()) {
          placeExtraBoss(msg.getExtraBoss().getPath());
        } else {
          removeExtraBoss();
        }

        placedAttackCardsLimit = msg.getPlacedAttackCardsLimit();
        placedDefenseCardsLimit = msg.getPlacedDefenseCardsLimit();
        // TODO: make placement limits visible to the player at all times
      }
      case START_GAME -> {
        host = true;
      }
      case ANNOUNCE_NEW_ROUND -> {
        placePlayingCards();
        client.playingCardsTable.setVisible(true);
      }
      case SHOW_DIALOG -> {
        switch (msg.getDialog().getType()) {
          case THREE_GEMS -> showThreeGemsDialog();
          case TRADE -> showTradeDialog();
        }
      }
      case ANNOUNCE_GAME_END -> {
        // TODO: highlight players with most gems of given gem color according to gem score cards.
        // TODO: show player match ranking (1st, 2nd, ...)
        client.localPlayerStatsTable.setVisible(false);
        client.remotePlayerStatsTable.setVisible(false);
        client.playingCardsTable.setVisible(false);
        client.extraBossTable.setVisible(false);
        client.vaultCardsTable.setVisible(false);

        updateGameEndTable(msg.getPlayerStatsList());
        client.gameEndTable.setVisible(true);
      }
      case ERROR_MESSAGE -> {
        log.error(msg.getErrorMessage());
        // TODO: display the error message to the player
        Gdx.app.exit();
      }
      default -> log.error("Unexpected message type: {}", msg.getType());
    }
  }

  private void updatePlayer(Message.PlayerStats stats) {
    Optional<Player> foundPlayer = findPlayer(stats.getColor());
    Player player =
        foundPlayer.orElseGet(
            () -> {
              Player p = new Player(stats.getColor());
              players.add(p);
              return p;
            });

    if (player.getColor().equals(client.color)) {
      localPlayer = player;
      localPlayer.setUpdated(true);
    }

    if (stats.hasTotalHiddenLootCardCount()) {
      player.setTotalHiddenLootCardCount(stats.getTotalHiddenLootCardCount());
    }

    if (stats.hasPoints()) {
      player.setPoints(stats.getPoints());
    }

    for (Gem gem : stats.getGemsList()) {
      Gem.Color key = gem.getColor();
      int oldValue = player.getGems().getOrDefault(key, 0);
      int newValue = gem.getAmount();
      player.getGems().put(key, newValue);
      log.info(
          "Set {} gems of player {} to {} (was {}).", key, player.getColor(), newValue, oldValue);
    }

    stats
        .getHiddenLootCardsList()
        .forEach(
            card ->
                player
                    .getHiddenLootCards()
                    .put(
                        card.getColor(),
                        player.getHiddenLootCards().getOrDefault(card.getColor(), 0)
                            + card.getAmount()));

    if (player != localPlayer) {
      updateRemotePlayerStatsTable(player);
    }
  }

  private void updateRemotePlayerStatsTable(Player player) {
    log.debug("Updating remote player stats table.");
    client.remotePlayerStatsTable.update(player);
  }

  private void initLocations(int locationCount) {
    log.debug("Initializing {} locations.", locationCount);
    for (int i = 0; i < locationCount; i++) {
      Location.Name name = Location.Name.values()[i];
      client.locations.add(
          new Location(
              name,
              new LocationCard(
                  new Image(
                      new Texture(
                          "cards/location/location_" + name.toString().toLowerCase() + ".png")))));
    }
  }

  private void placeLocationCards() {
    for (Location loc : client.locations) {
      client
          .vaultCardsTable
          .add(loc.getLocationCard().getImage())
          .padRight(VAULT_CARDS_TABLE_RIGHT_PAD);
    }
  }

  private void updateCardsInHand() {
    client.playingCardsTable.clear();
    for (PlayingCard card : playingCardsInHand) {
      client.playingCardsTable.add(card.getImage()).padRight(30f);
    }
  }

  private void updateCards() {
    client.vaultCardsTable.clear();

    log.info("Updating placed cards.");
    // defense cards
    placeDefenseCards();

    // location cards
    client.vaultCardsTable.row().padTop(30f);
    placeLocationCards();

    // vault cards
    client.vaultCardsTable.row().padTop(10f);
    placeVaultCards();

    // attack cards
    client.vaultCardsTable.row().padTop(30f);
    placeAttackCards();
  }

  private void placeDefenseCards() {
    for (Location loc : client.locations) {
      log.debug("Placing defense cards at location {}.", loc.getName());
      VerticalGroup verticalGroup = new VerticalGroup();
      verticalGroup.space(-130f).padRight(VAULT_CARDS_TABLE_RIGHT_PAD);

      // Place `blankCardsCount` of blank cards as placeholders first, and then place the actual
      // cards. It must be in this particular order to make sure the actual cards are displayed
      // consistently - not too high above the location card, and in a way that doesn't move the
      // whole table when resolving conflict.
      int blankCardsCount = PLAYER_LIMIT - loc.getDefenseCards().size();
      IntStream.range(0, blankCardsCount)
          .forEach(
              i -> {
                Image blankCardImage = new Image(client.blankPlayingCard);
                blankCardImage.setVisible(false);
                verticalGroup.addActor(blankCardImage);
              });

      loc.getDefenseCards().forEach(card -> verticalGroup.addActor(card.getImage()));
      client.vaultCardsTable.add(verticalGroup);
    }
  }

  private void placeAttackCards() {
    for (Location loc : client.locations) {
      log.debug("Placing attack cards at location {}.", loc.getName());
      VerticalGroup verticalGroup = new VerticalGroup();
      verticalGroup.space(-130f).padRight(VAULT_CARDS_TABLE_RIGHT_PAD);

      // Place the actual cards first, and then place `blankCardsCount` of blank cards as
      // placeholders. It must be in this particular order to make sure the actual cards are
      // displayed consistently - not too low below the location card, and in a way that doesn't
      // move the whole table when resolving conflict.
      loc.getAttackCards().forEach(card -> verticalGroup.addActor(card.getImage()));
      int blankCardsCount = PLAYER_LIMIT - loc.getAttackCards().size();
      IntStream.range(0, blankCardsCount)
          .forEach(
              i -> {
                Image blankCardImage = new Image(client.blankPlayingCard);
                blankCardImage.setVisible(false);
                verticalGroup.addActor(blankCardImage);
              });
      client.vaultCardsTable.add(verticalGroup);
    }
  }

  private void placeVaultCards() {
    for (Location loc : client.locations) {
      VaultCard card = loc.getVaultCard();
      Image image;
      if (card == null) {
        log.debug("Placing blank vault card at location {}.", loc.getName());
        image = new Image();
      } else {
        log.debug("Placing vault card at location {}.", loc.getName());
        image = card.getImage();
      }

      client.vaultCardsTable.add(image).padRight(VAULT_CARDS_TABLE_RIGHT_PAD);
    }
  }

  private void placePlayingCards() {
    client.playingCardsTable.clear();
    playingCardsInHand.clear();
    for (Location loc : client.locations) {
      Location.Name name = loc.getName();
      log.debug("Adding playing card {} to hand.", name);
      PlayingCard card =
          new PlayingCard(
              "cards/playing/playing_"
                  + client.color.toString().toLowerCase()
                  + "_"
                  + name.toString().toLowerCase()
                  + ".png",
              client.color,
              name,
              PlayingCard.State.IN_HAND);

      Image cardImage = card.getImage();
      cardImage.addListener(getPlayingCardListener(card));
      client.playingCardsTable.add(cardImage).padRight(30f);
      playingCardsInHand.add(card);
    }
    log.info("Playing cards placed.");
  }

  private ClickListener getPlayingCardListener(PlayingCard card) {
    return new ClickListener() {
      @Override
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        if (button == Input.Buttons.LEFT || button == Input.Buttons.RIGHT) {
          return true;
        }
        return super.touchDown(event, x, y, pointer, button);
      }

      @Override
      public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        super.touchUp(event, x, y, pointer, button);
        log.debug("Clicked playing card {}. State: {}.", card.name, card.getState());
        if (button == Input.Buttons.LEFT) {
          if (card.getState().equals(PlayingCard.State.IN_HAND)) {
            moveToAttack(card);
          } else {
            moveToHand(card);
          }
        } else if (button == Input.Buttons.RIGHT) {
          if (card.getState().equals(PlayingCard.State.IN_HAND)) {
            moveToDefense(card);
          } else {
            moveToHand(card);
          }
        }
      }
    };
  }

  private Optional<Player> findPlayer(Message.PlayerColor playerColor) {
    for (Player p : players) {
      if (p.getColor().equals(playerColor)) {
        return Optional.of(p);
      }
    }
    return Optional.empty();
  }

  private int getPlacedAttackCardsCount() {
    return client.locations.stream()
        .mapToInt(
            loc ->
                loc.getAttackCards().stream()
                    .filter(card -> PlayingCard.State.ATTACK.equals(card.getState()))
                    .toList()
                    .size())
        .sum();
  }

  private int getPlacedDefenseCardsCount() {
    return client.locations.stream()
        .mapToInt(
            loc ->
                loc.getDefenseCards().stream()
                    .filter(card -> PlayingCard.State.DEFENSE.equals(card.getState()))
                    .toList()
                    .size())
        .sum();
  }

  private void moveToAttack(PlayingCard card) {
    boolean moved = false;
    for (Location loc : client.locations) {
      if (card.name.equals(loc.getName()) && getPlacedAttackCardsCount() < placedAttackCardsLimit) {
        List<PlayingCard> attackCards = loc.getAttackCards();
        attackCards.add(card);
        loc.setAttackCards(attackCards);
        card.setState(PlayingCard.State.ATTACK);
        playingCardsInHand.remove(card);
        moved = true;
        log.debug("Card {} moved to attack.", card.name);

        checkEndTurnButtonStatus();
        break;
      }
    }

    if (moved) {
      updateCards();
      updateCardsInHand();
    }
  }

  private void moveToDefense(PlayingCard card) {
    boolean moved = false;
    for (Location loc : client.locations) {
      if (card.name.equals(loc.getName())
          && getPlacedDefenseCardsCount() < placedDefenseCardsLimit) {
        List<PlayingCard> defenseCards = loc.getDefenseCards();
        defenseCards.add(card);
        loc.setDefenseCards(defenseCards);
        card.setState(PlayingCard.State.DEFENSE);
        playingCardsInHand.remove(card);
        moved = true;
        log.debug("Card {} moved to defense.", card.name);

        checkEndTurnButtonStatus();
        break;
      }
    }

    if (moved) {
      updateCards();
      updateCardsInHand();
    }
  }

  private void moveToHand(PlayingCard card) {
    boolean moved = false;
    for (Location loc : client.locations) {
      if (card.name.equals(loc.getName())) {
        switch (card.getState()) {
          case ATTACK -> loc.getAttackCards().remove(card);
          case DEFENSE -> loc.getDefenseCards().remove(card);
        }
        playingCardsInHand.add(card);
        playingCardsInHand.sort(Comparator.comparing(c -> c.name));
        card.setState(PlayingCard.State.IN_HAND);
        moved = true;
        log.debug("Card {} moved to hand.", card.name);

        checkEndTurnButtonStatus();
        break;
      }
    }

    if (moved) {
      updateCards();
      updateCardsInHand();
    }
  }

  private void placeExtraBoss(String cardPath) {
    log.info("Placing extra boss.");
    client.extraBossTable.add(new Image(new Texture(cardPath)));
  }

  private void removeExtraBoss() {
    log.info("Removing extra boss.");
    client.extraBossTable.clear();
  }

  private void initGemIcons() {
    log.debug("Initializing gem icons.");
    for (Gem.Color gemColor : Gem.Color.values()) {
      if (gemColor.equals(Gem.Color.UNRECOGNIZED)) {
        // Ignore the UNRECOGNIZED enum value for obvious reasons.
        continue;
      }

      Texture texture =
          new Texture("icons/gems/gem_%s.png".formatted(gemColor.toString().toLowerCase()));
      client.gemTextures.add(texture);
      gemIcons.add(new Image(texture));
    }
  }

  private void updateLocalPlayerStatsTable() {
    log.debug("Updating local player stats table.");
    client.localPlayerStatsTable.clear();

    // Place gem icons and amounts.
    for (int i = 0; i < gemIcons.size(); i++) {
      client.localPlayerStatsTable.add(gemIcons.get(i)).padRight(VAULT_CARDS_TABLE_RIGHT_PAD);

      Label gemCount =
          new Label(
              String.valueOf(localPlayer.getGems().getOrDefault(Gem.Color.forNumber(i), 0)),
              client.blackLabelStyle);
      client.localPlayerStatsTable.add(gemCount).padLeft(15f).row();
    }

    // Place hidden loot card icon and amount.
    client
        .localPlayerStatsTable
        .add(new Image(client.hiddenLootCardTexture))
        .padRight(VAULT_CARDS_TABLE_RIGHT_PAD);
    client
        .localPlayerStatsTable
        .add(
            new Label(
                String.valueOf(localPlayer.getTotalHiddenLootCardCount()), client.blackLabelStyle))
        .padLeft(15f);

    // Place point amount.
    client.localPlayerStatsTable.row();
    client
        .localPlayerStatsTable
        .add(new Label("Pts", client.blackLabelStyle))
        .padRight(VAULT_CARDS_TABLE_RIGHT_PAD);
    client
        .localPlayerStatsTable
        .add(new Label(String.valueOf(localPlayer.getPoints()), client.blackLabelStyle))
        .padLeft(15f);
  }

  private void checkEndTurnButtonStatus() {
    // Show the button only when limits of placed att/def cards are met.
    boolean areLimitsMet =
        getPlacedAttackCardsCount() == placedAttackCardsLimit
            && getPlacedDefenseCardsCount() == placedDefenseCardsLimit;
    client.endTurnButtonTable.setVisible(areLimitsMet);
  }

  private void showThreeGemsDialog() {
    ThreeGemsDialog dialog =
        new ThreeGemsDialog(
            client.skin, client.whiteLabelStyle, client.gameStage, client.gemTextures);
    dialog.show();
  }

  private void showTradeDialog() {
    TradeDialog dialog =
        new TradeDialog(
            client.skin,
            client.whiteLabelStyle,
            client.gameStage,
            client.gemTextures,
            players,
            localPlayer);
    dialog.show();
  }

  private void updateGameEndTable(List<Message.PlayerStats> playerStatsList) {
    log.info("Updating game end table.");
    client.gameEndTable.clear();

    log.debug(
        "Winners: {}",
        playerStatsList.stream()
            .filter(Message.PlayerStats::getWon)
            .map(Message.PlayerStats::getColor)
            .toList());

    for (Message.PlayerStats stats : playerStatsList) {
      // One player's stats table layout:
      //   <Winner>
      // <Player color>
      //   _|_|<HLC icon>|Score
      // Gem|x|+HLC      |y
      // Gem|x|+HLC      |y
      // Gem|x|+HLC      |y
      // Gem|x|+HLC      |y
      // Pts|_|_         |y
      // Sum|_|_         |z
      //
      // "_" means an empty cell
      // "x" is a number of gems of given color
      // "y" is a final score gained from each gem color or from points
      // "z" is a sum of "y"'s

      VisTable gemTable = new VisTable();
      gemTable.setSkin(client.skin);
      gemTable
          .add(new Label(stats.getWon() ? "Winner" : "", client.blackLabelStyle))
          .colspan(4)
          .row();
      gemTable.add(new Label(stats.getColor().toString(), client.blackLabelStyle)).colspan(4).row();
      gemTable.addSeparator().colspan(4);
      gemTable.add();
      gemTable.add();
      gemTable.add(new Image(client.hiddenLootCardTexture)).padRight(GAME_END_TABLE_RIGHT_PAD);
      gemTable.add(new Label("Score", client.blackLabelStyle));
      gemTable.row();
      gemTable.addSeparator().colspan(4);

      Arrays.stream(Gem.Color.values())
          .filter(gemColor -> !gemColor.equals(Gem.Color.UNRECOGNIZED))
          .forEachOrdered(
              gemColor -> {
                // Find a matching gem color in `stats`.
                Optional<Gem> gemOptional =
                    stats.getGemsList().stream()
                        .filter(gem -> gem.getColor().equals(gemColor))
                        .findFirst();

                // Place gem icon.
                gemTable
                    .add(new Image(client.gemTextures.get(gemColor.ordinal())))
                    .padRight(GAME_END_TABLE_RIGHT_PAD);

                // Place gem amount. If a matching gem color above wasn't found, show zero gems.
                gemTable
                    .add(
                        new Label(
                            gemOptional.map(gem -> String.valueOf(gem.getAmount())).orElse("0"),
                            client.blackLabelStyle))
                    .padRight(GAME_END_TABLE_RIGHT_PAD);

                // Place (non-zero) hidden loot card count of this gem's color prepended by "+".
                Optional<Message.HiddenLootCard> hiddenLootCardOptional =
                    stats.getHiddenLootCardsList().stream()
                        .filter(hlc -> hlc.getColor().equals(gemColor))
                        .findFirst();
                gemTable
                    .add(
                        new Label(
                            hiddenLootCardOptional
                                .map(hiddenLootCard -> "+" + hiddenLootCard.getAmount())
                                .orElse(""),
                            client.blackLabelStyle))
                    .padRight(GAME_END_TABLE_RIGHT_PAD);

                // Show score for having given number of gems.
                stats.getScoresList().stream()
                    .filter(score -> score.getGemColor().equals(gemColor))
                    .findFirst()
                    .ifPresent(
                        score ->
                            gemTable
                                .add(
                                    new Label(
                                        String.valueOf(score.getAmount()), client.blackLabelStyle))
                                .padRight(GAME_END_TABLE_RIGHT_PAD));
                gemTable.row();
                gemTable.addSeparator().colspan(4);
              });

      // Place point amount.
      gemTable.add(new Label("Pts", client.blackLabelStyle)).padRight(GAME_END_TABLE_RIGHT_PAD);
      gemTable.add();
      gemTable.add();
      gemTable
          .add(new Label(String.valueOf(stats.getPoints()), client.blackLabelStyle))
          .padRight(GAME_END_TABLE_RIGHT_PAD);
      gemTable.row();
      gemTable.addSeparator().colspan(4);

      // Place score sum.
      gemTable.add(new Label("Sum", client.blackLabelStyle)).padRight(GAME_END_TABLE_RIGHT_PAD);
      gemTable.add();
      gemTable.add();
      gemTable
          .add(
              new Label(
                  String.valueOf(
                      stats.getScoresList().stream()
                              .mapToInt(Message.PlayerStats.Score::getAmount)
                              .sum()
                          + stats.getPoints()),
                  client.blackLabelStyle))
          .padRight(GAME_END_TABLE_RIGHT_PAD);

      // Add the gem table to a surrounding per-player table.
      VisTable statsTable = new VisTable();
      statsTable.pad(0, 50, 0, 50).setSkin(client.skin);
      statsTable.add(gemTable);

      client.gameEndTable.add(statsTable);
    }

    // Add a Leave Game button under the stats tables.
    client.gameEndTable.row();
    client
        .gameEndTable
        .add(client.leaveGameButton)
        .colspan(client.gameEndTable.getColumns())
        .pad(40f);
  }

  private void updateWaitingLabel() {
    client.waitingLabel.setVisible(true);
    client.waitingLabel.setText(
        "Waiting for more players ... (%d/%d)".formatted(playerCount, PLAYER_LIMIT));
  }
}
