# Licenses of used assets

- All images in the [cards](../cards) and [icons](../icons) directories are subject to the
[CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/) license.
- Droid Serif font is licensed under the [Apache License](apache-2.0-license.txt).
