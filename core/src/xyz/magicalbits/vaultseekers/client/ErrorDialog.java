/**
 * Vault Seekers Client
 * Copyright (C) 2023  MagicalBits.xyz
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package xyz.magicalbits.vaultseekers.client;


import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Align;

public class ErrorDialog extends GameDialog {
  private final String errorMessage;

  public ErrorDialog(Skin skin, Label.LabelStyle labelStyle, Stage stage, String errorMessage) {
    super(skin, labelStyle, stage);
    this.errorMessage = errorMessage;
  }

  @Override
  public void show() {
    dialog = new Dialog("Connection error", skin);
    dialog.setMovable(false);

    Button okButton = new Button(skin);
    okButton.add(new Label("ok", skin));
    okButton.addListener(getOkButtonListener());

    dialog.getTitleLabel().setAlignment(Align.center);
    dialog.getContentTable().add(new Label(errorMessage, labelStyle));
    dialog.getButtonTable().add(okButton);

    dialog.show(stage);
  }

  private InputListener getOkButtonListener() {
    return new InputListener() {
      @Override
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        return true;
      }

      @Override
      public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        closeDialog();
      }
    };
  }
}
