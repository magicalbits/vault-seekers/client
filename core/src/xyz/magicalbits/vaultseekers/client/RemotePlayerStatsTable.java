/**
 * Vault Seekers Client
 * Copyright (C) 2024  MagicalBits.xyz
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package xyz.magicalbits.vaultseekers.client;


import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class RemotePlayerStatsTable extends Table {
  private final Label.LabelStyle labelStyle;
  private final List<Texture> gemTextures;
  private final Texture hiddenLootCardTexture;
  private final Map<Player, PlayerStatsCard> statsCardMap = new HashMap<>();

  public RemotePlayerStatsTable(
      Skin skin,
      Label.LabelStyle labelStyle,
      List<Texture> gemTextures,
      Texture hiddenLootCardTexture) {
    super(skin);
    this.labelStyle = labelStyle;
    this.gemTextures = gemTextures;
    this.hiddenLootCardTexture = hiddenLootCardTexture;
  }

  public void update(Player player) {
    // This clear() call is necessary to keep only the active stats cards in the table.
    clear();
    statsCardMap.computeIfAbsent(
        player,
        p -> new PlayerStatsCard(p, getSkin(), labelStyle, gemTextures, hiddenLootCardTexture));
    statsCardMap.get(player).update();
    statsCardMap.values().forEach(card -> add(card.getRootTable()).padBottom(25f).row());
  }
}
