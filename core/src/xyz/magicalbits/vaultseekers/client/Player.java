/**
 * Vault Seekers Client
 * Copyright (C) 2024  MagicalBits.xyz
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package xyz.magicalbits.vaultseekers.client;


import java.util.EnumMap;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;
import xyz.magicalbits.vaultseekers.common.proto.Gem;
import xyz.magicalbits.vaultseekers.common.proto.Message;

@Getter
@Setter
public class Player {
  private final Message.PlayerColor color;
  private final Map<Gem.Color, Integer> gems = new EnumMap<>(Gem.Color.class);
  private final Map<Gem.Color, Integer> hiddenLootCards = new EnumMap<>(Gem.Color.class);
  private int totalHiddenLootCardCount = 0;
  private int points = 0;
  private boolean updated = false;

  @Override
  public String toString() {
    return "Player{"
        + "color="
        + color
        + ", gems="
        + gems
        + ", hiddenLootCards="
        + hiddenLootCards
        + ", totalHiddenLootCardCount="
        + totalHiddenLootCardCount
        + ", points="
        + points
        + ", updated="
        + updated
        + '}';
  }

  public Player(Message.PlayerColor color) {
    this.color = color;
  }
}
