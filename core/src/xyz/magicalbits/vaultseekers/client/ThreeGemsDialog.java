/**
 * Vault Seekers Client
 * Copyright (C) 2023  MagicalBits.xyz
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package xyz.magicalbits.vaultseekers.client;

import static xyz.magicalbits.vaultseekers.client.GameClient.GEM_COLOR_COUNT;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import lombok.extern.log4j.Log4j2;
import xyz.magicalbits.vaultseekers.common.proto.Gem;
import xyz.magicalbits.vaultseekers.common.proto.Message;

@Log4j2
public class ThreeGemsDialog extends GemDialog {
  private int totalChosenGems = 0;
  protected final Map<Gem.Color, Integer> chosenThreeGems = new EnumMap<>(Gem.Color.class);

  public ThreeGemsDialog(
      Skin skin, Label.LabelStyle labelStyle, Stage stage, List<Texture> gemTextures) {
    super(skin, labelStyle, stage, gemTextures);
  }

  @Override
  public void show() {
    chosenThreeGems.clear();

    dialog = new Dialog("", skin);
    dialog.setMovable(false);

    setTitle();

    Table gemTable = createGemTable();
    dialog.getContentTable().add(gemTable);

    confirmButton = createConfirmButton();
    dialog.getButtonTable().add(confirmButton);

    dialog.show(stage);
  }

  protected Table createGemTable() {
    Table gemTable = new Table(skin);
    List<Label> countLabels = new ArrayList<>(GEM_COLOR_COUNT);

    // Add plus buttons.
    for (int i = 0; i < GEM_COLOR_COUNT; i++) {
      Button plusButton = new Button(skin);
      plusButton.add(new Label("+", labelStyle)).pad(10);
      plusButton.addListener(
          getGemButtonListener(Gem.Color.values()[i], plusButton, true, countLabels));
      gemTable.add(plusButton).pad(10);
    }
    gemTable.row();

    // Add gem icons.
    for (int i = 0; i < GEM_COLOR_COUNT; i++) {
      gemTable.add(new Image(gemTextures.get(i))).pad(10);
    }
    gemTable.row();

    // Add minus buttons.
    for (int i = 0; i < GEM_COLOR_COUNT; i++) {
      Button minusButton = new Button(skin);
      minusButton.add(new Label("-", labelStyle)).pad(10);
      minusButton.addListener(
          getGemButtonListener(Gem.Color.values()[i], minusButton, false, countLabels));
      gemTable.add(minusButton);
    }
    gemTable.row();

    // Add gem counts.
    for (int i = 0; i < GEM_COLOR_COUNT; i++) {
      Label label = new Label("0", labelStyle);
      gemTable.add(label);
      countLabels.add(label);
    }
    return gemTable;
  }

  private InputListener getGemButtonListener(
      Gem.Color gemColor, Button buttonClicked, boolean isPlus, List<Label> countLabels) {
    return new InputListener() {
      @Override
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        return true;
      }

      @Override
      public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        log.debug(
            "Gem button '{}' clicked. {} {} {} {} {}",
            gemColor.toString(),
            x,
            y,
            pointer,
            button,
            buttonClicked.isDisabled());

        float buttonWidth = buttonClicked.getWidth();
        float buttonHeight = buttonClicked.getHeight();
        boolean releasedInBounds = x >= 0 && x <= buttonWidth && y >= 0 && y <= buttonHeight;
        if (!releasedInBounds) {
          return;
        }

        final int oldValue = chosenThreeGems.getOrDefault(gemColor, 0);
        if (isPlus && totalChosenGems == 3 || !isPlus && (totalChosenGems == 0 || oldValue == 0)) {
          // Return if the player attempts to choose more than three total gems or less than
          // zero gems.
          return;
        }

        final int newValue;
        if (isPlus) {
          newValue = oldValue + 1;
          totalChosenGems++;
        } else {
          newValue = oldValue - 1;
          totalChosenGems--;
        }

        if (newValue == 0) {
          chosenThreeGems.remove(gemColor);
        } else {
          chosenThreeGems.put(gemColor, newValue);
        }
        countLabels.get(gemColor.ordinal()).setText(newValue);
      }
    };
  }

  @Override
  protected InputListener getConfirmButtonListener(Button buttonClicked) {
    return new InputListener() {
      @Override
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        return true;
      }

      @Override
      public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        log.debug(
            "Confirm button clicked. {} {} {} {} {}",
            x,
            y,
            pointer,
            button,
            buttonClicked.isDisabled());

        float buttonWidth = buttonClicked.getWidth();
        float buttonHeight = buttonClicked.getHeight();
        if (x >= 0
            && x <= buttonWidth
            && y >= 0
            && y <= buttonHeight
            && !buttonClicked.isDisabled()) {
          buttonClicked.setDisabled(true);
          closeDialog();

          // Send chosen gems to the server.
          GameClient.getNetworkClient()
              .sendMessage(
                  Message.newBuilder()
                      .setType(Message.Type.SHOW_DIALOG)
                      .setDialog(
                          Message.Dialog.newBuilder()
                              .setType(Message.Dialog.Type.THREE_GEMS)
                              .build())
                      .addPlayerStats(
                          Message.PlayerStats.newBuilder()
                              .addAllGems(convertChosenGems(chosenThreeGems))
                              .build())
                      .build());
        }
      }
    };
  }
}
